﻿using IAsset.Test.Weather.Services;
using System.Web.Mvc;

namespace IAsset.Test.Weather.Controllers
{
    public class WeatherController : Controller
    {
        private IWeatherService _weatherService;

        public WeatherController(IWeatherService weatherService)
        {
            _weatherService = weatherService;
        }

        public JsonResult GetCities(string country)
        {
            var response = _weatherService.GetCities(country);
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetWeather(string country, string city)
        {
            var response = _weatherService.GetWeather(country, city);
            return Json(response, JsonRequestBehavior.AllowGet);
        }
    }
}