﻿using System.Web.Mvc;

namespace IAsset.Test.Weather.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }
    }
}