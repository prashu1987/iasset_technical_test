﻿using Autofac;
using Autofac.Integration.Mvc;
using GlobalWeather.Service_References;
using IAsset.Test.Weather.Services;
using System.Web.Mvc;
using System.Configuration;

namespace IAsset.Test.Weather
{
    public class IoCConfig
    {
        public static IContainer _container;

        public static IContainer Register()
        {
            var builder = new ContainerBuilder();

            builder.RegisterType<GlobalWeatherSoapClient>()
                .AsSelf()
                .WithParameter("endpointConfigurationName", "WeatherServiceEndpoint");

            var isStubsEnabled = ConfigurationManager.AppSettings.Get("IS_STUBS_ON") == "Y";
            if (isStubsEnabled)
            {
                builder.RegisterType<MockWeatherService>().As<IWeatherService>();
            }
            else
            {
                builder.RegisterType<WeatherService>().As<IWeatherService>();
            }

            builder.RegisterControllers(typeof(MvcApplication).Assembly);

            _container = builder.Build();

            DependencyResolver.SetResolver(new AutofacDependencyResolver(_container));

            return _container;
        }
    }
}