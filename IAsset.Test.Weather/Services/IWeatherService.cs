﻿namespace IAsset.Test.Weather.Services
{
    public interface IWeatherService
    {
        object GetCities(string country);

        object GetWeather(string country, string city);
    }
}