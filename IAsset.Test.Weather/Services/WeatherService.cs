﻿using GlobalWeather.Service_References;

namespace IAsset.Test.Weather.Services
{
    public class WeatherService : IWeatherService
    {
        private GlobalWeatherSoapClient _webServiceClient;

        public WeatherService(GlobalWeatherSoapClient webServiceClient)
        {
            _webServiceClient = webServiceClient;
        }

        public object GetCities(string country)
        {
            return _webServiceClient.GetCitiesByCountry(country);
        }

        public object GetWeather(string country, string city)
        {
            return _webServiceClient.GetWeather(city, country);
        }
    }
}