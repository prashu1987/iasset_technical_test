﻿using GlobalWeather.Service_References;

namespace IAsset.Test.Weather.Services
{
    public class MockWeatherService : IWeatherService
    {
        public object GetCities(string country)
        {
            return "<NewDataSet> <Table> <Country>Australia</Country> <City>Archerfield Aerodrome</City> </Table> <Table> <Country>Australia</Country> <City>Amberley Aerodrome</City> </Table> <Table> <Country>Australia</Country> <City>Alice Springs Aerodrome</City> </Table> <Table> <Country>Australia</Country> <City>Brisbane Airport M. O</City> </Table> <Table> <Country>Australia</Country> <City>Coolangatta Airport Aws</City> </Table> <Table> <Country>Australia</Country> <City>Cairns Airport</City> </Table> <Table> <Country>Australia</Country> <City>Charleville Airport</City> </Table></NewDataSet>";
        }

        public object GetWeather(string country, string city)
        {
            return "<CurrentWeather> <Location>Sydney Airport, Australia (YSSY) 33-57S 151-11E 3M</Location> <Time>Mar 24, 2016 - 11:00 AM EDT / 2016.03.24 1500 UTC</Time> <Wind> from the N (350 degrees) at 6 MPH (5 KT):0</Wind> <Visibility> greater than 7 mile(s):0</Visibility> <Temperature> 71 F (22 C)</Temperature> <DewPoint> 60 F (16 C)</DewPoint> <RelativeHumidity> 68%</RelativeHumidity> <Pressure> 30.09 in. Hg (1019 hPa)</Pressure> <Status>Success</Status> </CurrentWeather>";
        }
    }
}