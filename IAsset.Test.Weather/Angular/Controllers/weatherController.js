﻿app.controller('weatherController', ['$scope', 'weatherService', function ($scope, weatherService) {
    $scope.pageTitle = 'Weather Information';
    $scope.weather = {};

    $scope.selectCountry = function (selectedCountry) {
        $scope.weather.country = selectedCountry;
        $scope.selectedCountry = selectedCountry;
        $scope.countries = [];

        getCities(selectedCountry);
    };

    $scope.selectCity = function (selectedCity) {
        $scope.weather.city = selectedCity;
        $scope.selectedCity = selectedCity;

        getWeather($scope.weather);
    };

    $scope.resetCountry = function () {
        $scope.weather.country = '';
        $scope.selectedCountry = '';
        $scope.countries = [];
        $scope.cities = [];

        $scope.resetCity();
    };

    $scope.resetCity = function () {
        $scope.weather.city = '';
        $scope.selectedCity = '';
    };

    $scope.$watch('weather.country', function (value) {
        if (!value || value === '' || value.length < 2 || $scope.weather.country === $scope.selectedCountry || $scope.isSearchingCountries)
            return;

        getCountries(value);
    });

    $scope.toggleHighlight = function (index) {
        $scope.highlightedIndex = index;
    };

    $scope.showCountryList = function () {
        return _.some($scope.countries);
    };

    $scope.showCityList = function () {
        return _.some($scope.cities) && (!$scope.weather.city || $scope.weather.city === '');
    };

    $scope.showCity = function () {
        var weather = $scope.weather;
        return !!weather.country && weather.country !== ''
            && (_.some($scope.cities)
                || (!!weather.city && weather.city !== '' && weather.city === $scope.selectedCity));
    };

    $scope.showWeather = function () {
        var weather = $scope.weather;
        return !!weather.country && weather.country !== ''
            && !!weather.city && weather.city !== ''
            && !!weather.details && weather.details.length > 0;
    };

    var getCountries = function (country) {
        $scope.isSearchingCountries = true;
        $scope.error = '';
        weatherService.getCountries(country)
            .then(getCountriesSuccess, function () {
                $scope.countries = [];
                $scope.isSearchingCountries = false;
                $scope.error = 'Something went wrong! Please try again later';
            });
    };

    var getCities = function (country) {
        $scope.error = ''
        weatherService.getCities(country)
            .then(getCitiesSuccess, function () {
                $scope.cities = [];
                $scope.error = 'Something went wrong! Please try again later';
            });
    };

    var getWeather = function (weather) {
        $scope.error = '';
        weatherService.getWeather(weather)
            .then(getWeatherSuccess, function () {
                $scope.weather.details = [];
                $scope.error = 'Something went wrong! Please try again later';
            });
    };

    var getCountriesSuccess = function (data) {
        $scope.countries = [];

        if (!data || !data.NewDataSet || data.NewDataSet.Table.length === 0) {
            $scope.error = 'No data found.';
            $scope.isSearchingCountries = false;
            return;
        }

        var filteredList = _.filter(data.NewDataSet.Table, function (table) {
            return _.startsWith(table.Country.toUpperCase(), $scope.weather.country.toUpperCase());
        });

        if (filteredList.length > 0) {
            _.each(filteredList, function (item) {
                if (!item && !item.Country) return;

                var exists = _.some($scope.countries, function (country) { return country === item.Country; });

                if (!exists)
                    $scope.countries.push(item.Country);
            });
        }
        else {
            $scope.error = 'No data found. Please review your search text.';
        }

        $scope.isSearchingCountries = false;
    };

    var getCitiesSuccess = function (data) {
        $scope.cities = [];

        if (!data || !data.NewDataSet || data.NewDataSet.Table.length === 0) {
            $scope.error = 'No data found. Please try again later.';
            return;
        }

        _.each(data.NewDataSet.Table, function (item) {
            if (!item && !item.City) return;
            $scope.cities.push(item.City);
        });
    };

    var getWeatherSuccess = function (data) {
        $scope.weather.details = [];

        if (!data || !data.CurrentWeather) {
            $scope.error = 'No data found. Please try again later.';
            return;
        }

        $scope.weather.details = _.map(data.CurrentWeather, function (value, name) {
            return { label: name, info: value };
        });
    };
}]);