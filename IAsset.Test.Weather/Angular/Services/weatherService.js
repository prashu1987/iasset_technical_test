﻿app.service('weatherService', ['$http', '$q', function ($http, $q) {
    var invokeApi = function (url) {
        var deferred = $q.defer();
        
        $http.get(url)
            .then(function (response) {
                var data = parseResposne(response.data);
                deferred.resolve(data);
                console.log('success: ');
                console.log(data);
            }, function (response) {
                deferred.reject(response);
                console.log('error: ');
                console.log(response);
            });

        return deferred.promise;
    };

    var parseResposne = function (data) {
        if (!data && data === "")
            return data;

        if (data[0] === "<") {
            var x2js = new X2JS();
            return x2js.xml_str2json(data);
        }
    };

    var getCountries = function (country) {
        return invokeApi('Weather/GetCities?country=' + country);
    };

    var getCities = function (country) {
        return invokeApi('Weather/GetCities?country=' + country);
    };

    var getWeather = function (weather) {
        return invokeApi('Weather/GetWeather?country=' + weather.country + '&city=' + weather.city);
    }

    return {
        getCountries: getCountries,
        getCities: getCities,
        getWeather: getWeather
    };
}]);