# Global Weather - Technical Exercise solution

The application allows the user to select a country and then a city that belongs to the selected country and shows up weather information.

### This solutions includes the below components:

- ASP.Net MVC project
- Angular front end
- JS Tests
- C# Tests

### Installation

- Nuget packages need to be restored
- Node modules need to be restored, please install node.js and below command should install all the node modules from package.json

```
npm install
```

### Run
- Open the solution in VS2015 or higher
- Press F5

## Tests

### JS tests have been written using

- karma as a runner
- jasmine as the framework
- angular mocks for managing dependency injection
- launches chrome browser (can be configured to user headless browsers like PhantomJS)

```
node_modules\.bin\karma start
```

##### This should give an output something similar to the below:

```
E:\Official\GlobalWeather\IAsset.Test.Weather>karma start
02 04 2017 13:49:02.015:INFO [karma]: Karma v0.13.22 server started at http://localhost:9876/
02 04 2017 13:49:02.027:INFO [launcher]: Starting browser Chrome
02 04 2017 13:49:03.494:INFO [Chrome 56.0.2924 (Windows 10 0.0.0)]: Connected on socket /#3S_j0ZG4QVh5sLyWAAAA with id 31022568
..................................
Chrome 56.0.2924 (Windows 10 0.0.0): Executed 34 of 34 SUCCESS (0.158 secs / 0.129 secs)
```

### .Net unit tests have been written using

- nunit framework
- NSubstitute for mocking


## Author
Author: Prashanth Sundareshan <prashus.prashanth@gmail.com>