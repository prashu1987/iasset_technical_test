﻿using IAsset.Test.Weather.Controllers;
using IAsset.Test.Weather.Services;
using NSubstitute;
using NUnit.Framework;
using System.Text;
using FluentAssertions;
using System.Web.Mvc;

namespace GlobalWeather.Tests
{
    [TestFixture]
    public class WeatherControllerTests
    {
        private WeatherController _controller;
        private IWeatherService mockWeatherService;

        [SetUp]
        public void Start()
        {
            mockWeatherService = Substitute.For<IWeatherService>();
            _controller = new WeatherController(mockWeatherService);
        }

        [TestCase("<NewDataSet><Table><Country>United States</Country><City>Escanaba</City></Table><Table><Country>United States</Country><City>Shannon Airport</City></Table></NewDataSet>")]
        [TestCase("<NewDataSet><Table></Table></NewDataSet>")]
        [TestCase("")]
        public void Should_Get_Cities_By_Country(string response)
        {
            mockWeatherService.GetCities(Arg.Any<string>()).Returns(response);

            var json = _controller.GetCities("United States");

            Assert.That(json.GetType(), Is.EqualTo(typeof(JsonResult)));
            Assert.That(json.Data, Is.EqualTo(response));
        }

        [TestCase("<CurrentWeather> <Location>Sydney Airport, Australia (YSSY) 33-57S 151-11E 3M</Location> <Time>Mar 24, 2016 - 11:00 AM EDT / 2016.03.24 1500 UTC</Time> <Wind> from the N (350 degrees) at 6 MPH (5 KT):0</Wind> <Visibility> greater than 7 mile(s):0</Visibility> <Temperature> 71 F (22 C)</Temperature> <DewPoint> 60 F (16 C)</DewPoint> <RelativeHumidity> 68%</RelativeHumidity> <Pressure> 30.09 in. Hg (1019 hPa)</Pressure> <Status>Success</Status> </CurrentWeather>")]
        [TestCase("")]
        public void Should_Get_Weather_For_Given_City_Country(string response)
        {
            mockWeatherService.GetWeather(Arg.Any<string>(), Arg.Any<string>()).Returns(response);

            var json = _controller.GetWeather("Australia", "Sydney");

            Assert.That(json.GetType(), Is.EqualTo(typeof(JsonResult)));
            Assert.That(json.Data, Is.EqualTo(response));
        }

        [TearDown]
        public void End()
        {
            _controller.Dispose();
            mockWeatherService = null;
        }
    }
}