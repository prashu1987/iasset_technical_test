﻿describe('weatherController', function () {
    var scope, mockWeatherService, controller, citiesDeferred, weatherDeferred, defer;

    var rawCountriesResponse = JSON.parse('{"NewDataSet":{"Table":[{"Country":"Benin","City":"Cotonou"},{"Country":"Benin","City":"Bohicon"},{"Country":"Benin","City":"Kandi"},{"Country":"Benin","City":"Natitingou"},{"Country":"Benin","City":"Parakou"},{"Country":"Benin","City":"Save"},{"Country":"Burkina Faso","City":"Ouahigouya"},{"Country":"Burkina Faso","City":"Boromo"},{"Country":"Burkina Faso","City":"Po"},{"Country":"Burkina Faso","City":"Dori"},{"Country":"Burkina Faso","City":"Fada N Gourma"},{"Country":"Burkina Faso","City":"Ouagadougou"},{"Country":"Burkina Faso","City":"Dedougou"},{"Country":"Burkina Faso","City":"Gaoua"},{"Country":"Burkina Faso","City":"Bobo-Dioulasso"},{"Country":"Finland","City":"Halli"},{"Country":"Finland","City":"Helsinki-Malmi"},{"Country":"Finland","City":"Helsinki-Vantaa"},{"Country":"Finland","City":"Hailuoto"},{"Country":"Finland","City":"Ahtari"},{"Country":"Finland","City":"Ivalo"},{"Country":"Finland","City":"Joensuu"},{"Country":"Finland","City":"Jyvaskyla"},{"Country":"Finland","City":"Kauhava"},{"Country":"Finland","City":"Kemi"},{"Country":"Finland","City":"Kajaani"},{"Country":"Finland","City":"Kruunupyy"},{"Country":"Finland","City":"Kuusamo"},{"Country":"Finland","City":"Kuopio"},{"Country":"Finland","City":"Lappeenranta"},{"Country":"Finland","City":"Mariehamn / Aland Island"},{"Country":"Finland","City":"Mikkeli"},{"Country":"Finland","City":"Oulu"},{"Country":"Finland","City":"Pello"},{"Country":"Finland","City":"Pori"},{"Country":"Finland","City":"Pudasjarvi"},{"Country":"Finland","City":"Rovaniemi"},{"Country":"Finland","City":"Savonlinna"},{"Country":"Finland","City":"Sodankyla"},{"Country":"Finland","City":"Suomussalmi"},{"Country":"Finland","City":"Tampere / Pirkkala"},{"Country":"Finland","City":"Turku"},{"Country":"Finland","City":"Utti"},{"Country":"Finland","City":"Vaasa"},{"Country":"Finland","City":"Viitasaari"},{"Country":"United Kingdom","City":"Belfast / Aldergrove Airport"},{"Country":"United Kingdom","City":"Belfast / Harbour"},{"Country":"United Kingdom","City":"Eglinton / Londonderr"},{"Country":"United Kingdom","City":"Birmingham / Airport"},{"Country":"United Kingdom","City":"Derby"},{"Country":"United Kingdom","City":"Coventry Airport"},{"Country":"United Kingdom","City":"Leicester"},{"Country":"United Kingdom","City":"Staverton Private"},{"Country":"United Kingdom","City":"Northampton / Sywell"},{"Country":"United Kingdom","City":"Tatenhill"},{"Country":"United Kingdom","City":"Nottingham"},{"Country":"United Kingdom","City":"Halfpenny Green"},{"Country":"United Kingdom","City":"Shobdon"},{"Country":"United Kingdom","City":"Turweston"},{"Country":"United Kingdom","City":"Wellesbourne Mountford"},{"Country":"United Kingdom","City":"Manchester / Barton"},{"Country":"United Kingdom","City":"Manchester Airport"},{"Country":"United Kingdom","City":"Woodford"},{"Country":"United Kingdom","City":"Sandtoft"},{"Country":"United Kingdom","City":"Sherburn-In-Elmet"},{"Country":"United Kingdom","City":"Caernarfon"},{"Country":"United Kingdom","City":"Fenland"},{"Country":"United Kingdom","City":"Sturgate"},{"Country":"United Kingdom","City":"Sleap"},{"Country":"United Kingdom","City":"Welshpool"},{"Country":"United Kingdom","City":"Brawdy"},{"Country":"United Kingdom","City":"Plymouth"},{"Country":"United Kingdom","City":"Chivenor"},{"Country":"United Kingdom","City":"Saint Mawgan"},{"Country":"United Kingdom","City":"Upavon"},{"Country":"United Kingdom","City":"Kemble"},{"Country":"United Kingdom","City":"Lyneham"},{"Country":"United Kingdom","City":"Boscombe Down"},{"Country":"United Kingdom","City":"Netheravon"},{"Country":"United Kingdom","City":"Predannack"},{"Country":"United Kingdom","City":"Portland / Rnas"},{"Country":"United Kingdom","City":"Culdrose"},{"Country":"United Kingdom","City":"Wroughton"},{"Country":"United Kingdom","City":"Merryfield"},{"Country":"United Kingdom","City":"St Athan"},{"Country":"United Kingdom","City":"Yeovilton"},{"Country":"United Kingdom","City":"Cardiff/tremorfa Heliport"},{"Country":"United Kingdom","City":"Haverfordwest"},{"Country":"United Kingdom","City":"Cardiff-Wales Airport"},{"Country":"United Kingdom","City":"Swansea"},{"Country":"United Kingdom","City":"Bristol / Lulsgate"},{"Country":"United Kingdom","City":"Liverpool / John Lennon Airport"},{"Country":"United Kingdom","City":"Luton Airport"},{"Country":"United Kingdom","City":"Compton Abbas"},{"Country":"United Kingdom","City":"Lands End / St Just"},{"Country":"United Kingdom","City":"Plymouth / Roborough"},{"Country":"United Kingdom","City":"Scilly, Saint Mary S"},{"Country":"United Kingdom","City":"Yeovil / Westland"},{"Country":"United Kingdom","City":"Bournemouth Airport"},{"Country":"United Kingdom","City":"Southampton / Weather Centre"},{"Country":"United Kingdom","City":"Bembridge"},{"Country":"United Kingdom","City":"Penzance Heliport"},{"Country":"United Kingdom","City":"Lasham"},{"Country":"United Kingdom","City":"Isle Of Wight / Sandown"},{"Country":"United Kingdom","City":"Thruxton"},{"Country":"United Kingdom","City":"Popham"},{"Country":"United Kingdom","City":"Chichester / Goodwood"},{"Country":"United Kingdom","City":"Alderney / Channel Island"},{"Country":"United Kingdom","City":"Guernsey Airport"},{"Country":"United Kingdom","City":"Jersey Airport"},{"Country":"United Kingdom","City":"Shoreham Airport"},{"Country":"United Kingdom","City":"Biggin Hill"},{"Country":"United Kingdom","City":"Bognor Regis"},{"Country":"United Kingdom","City":"Challock"},{"Country":"United Kingdom","City":"Lashenden / Headcorn"},{"Country":"United Kingdom","City":"London / Gatwick Airport"},{"Country":"United Kingdom","City":"Redhill"},{"Country":"United Kingdom","City":"Bodmin"},{"Country":"United Kingdom","City":"London City Airport"},{"Country":"United Kingdom","City":"Denham"},{"Country":"United Kingdom","City":"Farnborough"},{"Country":"United Kingdom","City":"Panshanger"},{"Country":"United Kingdom","City":"Blackbushe"},{"Country":"United Kingdom","City":"London / Heathrow Airport"},{"Country":"United Kingdom","City":"White Waltham"},{"Country":"United Kingdom","City":"Old Sarum"},{"Country":"United Kingdom","City":"London / Westland Heliport"},{"Country":"United Kingdom","City":"Fowlmere"},{"Country":"United Kingdom","City":"Southend-On-Sea"},{"Country":"United Kingdom","City":"Lydd Airport"},{"Country":"United Kingdom","City":"Manston Civil"},{"Country":"United Kingdom","City":"Hucknall"},{"Country":"United Kingdom","City":"Brough"},{"Country":"United Kingdom","City":"Carlisle"},{"Country":"United Kingdom","City":"Retford / Gamston"},{"Country":"United Kingdom","City":"Netherthorpe"},{"Country":"United Kingdom","City":"Samlesbury"},{"Country":"United Kingdom","City":"Blackpool Airport"},{"Country":"United Kingdom","City":"Humberside"},{"Country":"United Kingdom","City":"Barrow / Walney Island"},{"Country":"United Kingdom","City":"Leeds And Bradford"},{"Country":"United Kingdom","City":"Warton"},{"Country":"United Kingdom","City":"Hawarden"},{"Country":"United Kingdom","City":"Isle Of Man / Ronaldsway Airport"},{"Country":"United Kingdom","City":"Newcastle"},{"Country":"United Kingdom","City":"Tees-Side"},{"Country":"United Kingdom","City":"Wickenby"},{"Country":"United Kingdom","City":"East Midlands"},{"Country":"United Kingdom","City":"Llanbedr"},{"Country":"United Kingdom","City":"Ternhill"},{"Country":"United Kingdom","City":"Spadeadam"},{"Country":"United Kingdom","City":"Pembrey Sands"},{"Country":"United Kingdom","City":"Mona"},{"Country":"United Kingdom","City":"Shawbury"},{"Country":"United Kingdom","City":"Valley"},{"Country":"United Kingdom","City":"Woodvale"},{"Country":"United Kingdom","City":"West Freugh"},{"Country":"United Kingdom","City":"Kirkwall Airport"},{"Country":"United Kingdom","City":"Sumburgh Cape"},{"Country":"United Kingdom","City":"Wick"},{"Country":"United Kingdom","City":"Aberdeen / Dyce"},{"Country":"United Kingdom","City":"Inverness / Dalcross"},{"Country":"United Kingdom","City":"Glasgow Airport"},{"Country":"United Kingdom","City":"Edinburgh Airport"},{"Country":"United Kingdom","City":"Prestwick Airport"},{"Country":"United Kingdom","City":"Benbecula"},{"Country":"United Kingdom","City":"Scatsa / Shetland Island"},{"Country":"United Kingdom","City":"Dundee / Riverside"},{"Country":"United Kingdom","City":"Stornoway"},{"Country":"United Kingdom","City":"Tiree"},{"Country":"United Kingdom","City":"Unst Island"},{"Country":"United Kingdom","City":"Tain Range"},{"Country":"United Kingdom","City":"Machrihanish"},{"Country":"United Kingdom","City":"Kinloss"},{"Country":"United Kingdom","City":"Leuchars"},{"Country":"United Kingdom","City":"Boulmer"},{"Country":"United Kingdom","City":"Lossiemouth"},{"Country":"United Kingdom","City":"London Weather Centre"},{"Country":"United Kingdom","City":"Bracknell / Beaufort Park"},{"Country":"United Kingdom","City":"Shipdham"},{"Country":"United Kingdom","City":"Bedford / Castle Mill"},{"Country":"United Kingdom","City":"Cambridge"},{"Country":"United Kingdom","City":"Great Yarmouth / North Denes"},{"Country":"United Kingdom","City":"Ipswich"},{"Country":"United Kingdom","City":"Peterborough / Conington"},{"Country":"United Kingdom","City":"Stapleford"},{"Country":"United Kingdom","City":"Norwich Weather Centre"},{"Country":"United Kingdom","City":"Seething"},{"Country":"United Kingdom","City":"Hethel"},{"Country":"United Kingdom","City":"Andrewsfield"},{"Country":"United Kingdom","City":"Beccles ( Ellough )"},{"Country":"United Kingdom","City":"Bourn"},{"Country":"United Kingdom","City":"Crowfield"},{"Country":"United Kingdom","City":"Peterborough / Sibson"},{"Country":"United Kingdom","City":"Earls Colne"},{"Country":"United Kingdom","City":"Stansted Airport"},{"Country":"United Kingdom","City":"Duxford"},{"Country":"United Kingdom","City":"Bedford / Thurleigh"},{"Country":"United Kingdom","City":"Clacton"},{"Country":"United Kingdom","City":"North Weald"},{"Country":"United Kingdom","City":"Sheffield City"},{"Country":"United Kingdom","City":"Aylesbury / Thame"},{"Country":"United Kingdom","City":"Wycombe Air Park / Booker"},{"Country":"United Kingdom","City":"Cranfield"},{"Country":"United Kingdom","City":"Dunsfold"},{"Country":"United Kingdom","City":"Exeter Airport"},{"Country":"United Kingdom","City":"Fairoaks"},{"Country":"United Kingdom","City":"Filton / Bristol"},{"Country":"United Kingdom","City":"Hatfield"},{"Country":"United Kingdom","City":"Leavesden"},{"Country":"United Kingdom","City":"Oxford / Kidlington"},{"Country":"United Kingdom","City":"Rochester"},{"Country":"United Kingdom","City":"Perranporth"},{"Country":"United Kingdom","City":"Elstree"},{"Country":"United Kingdom","City":"Oaksey Park"},{"Country":"United Kingdom","City":"Benson"},{"Country":"United Kingdom","City":"Aberporth"},{"Country":"United Kingdom","City":"Farnborough Military"},{"Country":"United Kingdom","City":"Lakenheath"},{"Country":"United Kingdom","City":"Manston Military"},{"Country":"United Kingdom","City":"Mildenhall"},{"Country":"United Kingdom","City":"Colerne"},{"Country":"United Kingdom","City":"Lee On Solent"},{"Country":"United Kingdom","City":"Wattisham"},{"Country":"United Kingdom","City":"Wyton"},{"Country":"United Kingdom","City":"Fairford"},{"Country":"United Kingdom","City":"Brize Norton"},{"Country":"United Kingdom","City":"Odiham"},{"Country":"United Kingdom","City":"Middle Wallop"},{"Country":"United Kingdom","City":"Cosford"},{"Country":"United Kingdom","City":"Halton"},{"Country":"United Kingdom","City":"Northolt"},{"Country":"United Kingdom","City":"Alconbury Royal Air Force Base"},{"Country":"United Kingdom","City":"Coningsby"},{"Country":"United Kingdom","City":"Dishforth"},{"Country":"United Kingdom","City":"Leeming"},{"Country":"United Kingdom","City":"Church Fenton"},{"Country":"United Kingdom","City":"Honington"},{"Country":"United Kingdom","City":"Finningley"},{"Country":"United Kingdom","City":"Cottesmore"},{"Country":"United Kingdom","City":"Newton"},{"Country":"United Kingdom","City":"Scampton"},{"Country":"United Kingdom","City":"Wittering"},{"Country":"United Kingdom","City":"Linton-On-Ouse"},{"Country":"United Kingdom","City":"Leconfield"},{"Country":"United Kingdom","City":"Waddington"},{"Country":"United Kingdom","City":"Topcliffe"},{"Country":"United Kingdom","City":"Coltishall"},{"Country":"United Kingdom","City":"Cranwell"},{"Country":"United Kingdom","City":"Barkston Heath"},{"Country":"United Kingdom","City":"Holbeach"},{"Country":"United Kingdom","City":"Marham"},{"Country":"United Kingdom","City":"Wainfleet"},{"Country":"Equatorial Guinea","City":"Malabo/Fernando Poo"},{"Country":"Saint Helena","City":"Wide Awake Field Ascension Island"},{"Country":"British Indian Ocean Territory","City":"Diego Garcia"},{"Country":"Sao Tome and Principe","City":"Principe"},{"Country":"Sao Tome and Principe","City":"Sao Tome"},{"Country":"Spain","City":"Fuerteventura / Aeropuerto"},{"Country":"Spain","City":"Hierro / Aeropuerto"},{"Country":"Spain","City":"La Palma / Aeropuerto"},{"Country":"Spain","City":"Las Palmas De Gran Canaria / Gando"},{"Country":"Spain","City":"Lanzarote / Aeropuerto"},{"Country":"Spain","City":"Tenerife Sur"},{"Country":"Spain","City":"Tenerife / Los Rodeos"},{"Country":"Spain","City":"Melilla"},{"Country":"Guinea-Bissau","City":"Bafata"},{"Country":"Guinea-Bissau","City":"Bissau Aeroport"},{"Country":"Guinea","City":"Conakry / Gbessia"},{"Country":"Guinea","City":"Faranah / Badala"},{"Country":"Guinea","City":"Kindia"},{"Country":"Guinea","City":"Kissidougou"},{"Country":"Guinea","City":"Labe"},{"Country":"Guinea","City":"Macenta"},{"Country":"Guinea","City":"N Zerekore"},{"Country":"Guinea","City":"Boke"},{"Country":"Guinea","City":"Siguiri"},{"Country":"Guinea","City":"Kankan"},{"Country":"Spain","City":"Albacete / Los Llanos"},{"Country":"Spain","City":"Alicante / El Altet"},{"Country":"Spain","City":"Almeria / Aeropuerto"},{"Country":"Spain","City":"Asturias / Aviles"},{"Country":"Spain","City":"Cordoba / Aeropuerto"},{"Country":"Spain","City":"Bilbao / Sondica"},{"Country":"Spain","City":"Burgos / Villafria"},{"Country":"Spain","City":"Barcelona / Aeropuerto"},{"Country":"Spain","City":"Badajoz / Talavera La Real"},{"Country":"Spain","City":"Calamocha"},{"Country":"Spain","City":"La Coruna / Alvedro"},{"Country":"Spain","City":"Madri-Colmenar"},{"Country":"Spain","City":"Gerona / Costa Brava"},{"Country":"Spain","City":"Granada / Aeropuerto"},{"Country":"Spain","City":"Madrid / Getafe"},{"Country":"Spain","City":"Hinojosa Del Duque"},{"Country":"Spain","City":"Ibiza / Es Codola"},{"Country":"Spain","City":"Jerez De La Fronteraaeropuerto"},{"Country":"Spain","City":"Murcia / San Javier"},{"Country":"Spain","City":"Leon / Virgen Del Camino"},{"Country":"Spain","City":"Logrono / Agoncillo"},{"Country":"Spain","City":"Madrid / Barajas"},{"Country":"Spain","City":"Malaga / Aeropuerto"},{"Country":"Spain","City":"Menorca / Mahon"},{"Country":"Spain","City":"Moron De La Frontera"},{"Country":"Spain","City":"Palma De Mallorca / Son San Juan"},{"Country":"Spain","City":"Pamplona / Noain"},{"Country":"Spain","City":"Murcia / Alcantarilla"},{"Country":"Spain","City":"Reus / Aeropuerto"},{"Country":"Spain","City":"Rota"},{"Country":"Spain","City":"Salamanca / Matacan"},{"Country":"Spain","City":"San Sebastian / Fuenterrabia"},{"Country":"Spain","City":"Santiago / Labacolla"},{"Country":"Spain","City":"Madrid / Torrejon"},{"Country":"Spain","City":"Valencia / Aeropuerto"},{"Country":"Spain","City":"Valladolid / Villanubla"},{"Country":"Spain","City":"Madrid / Cuatro Vientos"},{"Country":"Spain","City":"Vitoria"},{"Country":"Spain","City":"Vigo / Peinador"},{"Country":"Spain","City":"Santander / Parayas"},{"Country":"Spain","City":"Zaragoza / Aeropuerto"},{"Country":"Spain","City":"Sevilla / San Pablo"},{"Country":"Saint Pierre and Miquelon","City":"Saint-Pierre"},{"Country":"Bosnia and Herzegovina","City":"Livno"},{"Country":"Dominican Republic","City":"Barahona"},{"Country":"Dominican Republic","City":"Herrera"},{"Country":"Dominican Republic","City":"La Romana International Airport"},{"Country":"Dominican Republic","City":"Punta Cana"},{"Country":"Dominican Republic","City":"Puerto Plata International"},{"Country":"Dominican Republic","City":"Las Americas"},{"Country":"Dominican Republic","City":"Santiago"},{"Country":"United States Minor Outlying Islands","City":"Pago Pago / Int. Airp."},{"Country":"Bahrain","City":"Bahrain International Airport"},{"Country":"United States Minor Outlying Islands","City":"Guam, Mariana Island"},{"Country":"United States Minor Outlying Islands","City":"Rota Intl / Rota Island"},{"Country":"United States Minor Outlying Islands","City":"Saipan / Isley Coast Guard Station"},{"Country":"United States Minor Outlying Islands","City":"Andersen Air Force Base"},{"Country":"United States Minor Outlying Islands","City":"Agana, Guam, Mariana Islands"},{"Country":"United States Minor Outlying Islands","City":"Peipeinimaru"},{"Country":"United States Minor Outlying Islands","City":"Johnston Island"},{"Country":"United States Minor Outlying Islands","City":"Wake Island Airfld"},{"Country":"Philippines","City":"Subic Bay Weather Station"},{"Country":"Philippines","City":"Laoag"},{"Country":"Philippines","City":"Ninoy Aquino Inter-National Airport"},{"Country":"Philippines","City":"Davao Airport"},{"Country":"Philippines","City":"Clark Ab"},{"Country":"Philippines","City":"Legaspi"},{"Country":"Philippines","City":"Romblon"},{"Country":"Philippines","City":"Sangley Point"},{"Country":"Philippines","City":"Mactan"},{"Country":"Philippines","City":"Zamboanga"},{"Country":"Philippines","City":"Aparri"},{"Country":"Philippines","City":"Baguio"},{"Country":"Philippines","City":"Daet"},{"Country":"Philippines","City":"San Jose"},{"Country":"Philippines","City":"Iba"},{"Country":"Philippines","City":"Calapan"},{"Country":"Philippines","City":"Basco"},{"Country":"Philippines","City":"Vigan"},{"Country":"Philippines","City":"Baler"},{"Country":"Philippines","City":"Tuguegarao"},{"Country":"Philippines","City":"Virac"},{"Country":"Philippines","City":"Tacloban"},{"Country":"Philippines","City":"Dumaguete"},{"Country":"Philippines","City":"Catarman"},{"Country":"Philippines","City":"Guiuan"},{"Country":"Philippines","City":"Iloilo"},{"Country":"Philippines","City":"Masbate"},{"Country":"Philippines","City":"Puerto Princesa"},{"Country":"Philippines","City":"Roxas"},{"Country":"Philippines","City":"Tagbilaran"},{"Country":"Philippines","City":"Gen. Santos"},{"Country":"Philippines","City":"Cotobato"},{"Country":"Philippines","City":"Butuan"},{"Country":"Philippines","City":"Dipolog"},{"Country":"Philippines","City":"Cagayan De Oro"},{"Country":"Philippines","City":"Surigao"},{"Country":"Philippines","City":"Malaybalay"},{"Country":"Philippines","City":"Alabat"},{"Country":"Argentina","City":"Concordia Aerodrome"},{"Country":"Argentina","City":"Gualeguaychu Aerodrome"},{"Country":"Argentina","City":"Junin Aerodrome"},{"Country":"Argentina","City":"Parana Aerodrome"},{"Country":"Argentina","City":"Rosario Aerodrome"},{"Country":"Argentina","City":"Villaguay Aerodrome"},{"Country":"Argentina","City":"Sauce Viejo Aerodrome"},{"Country":"Argentina","City":"Buenos Aires Observatorio"},{"Country":"Argentina","City":"Aeroparque Bs. As. Aerodrome"},{"Country":"Argentina","City":"Pilar Observatorio"},{"Country":"Argentina","City":"Cordoba Aerodrome"},{"Country":"Argentina","City":"Chepes"},{"Country":"Argentina","City":"Villa De Maria Del Rio Seco"},{"Country":"Argentina","City":"Don Torcuato Aerodrome"},{"Country":"Argentina","City":"La Plata Aerodrome"},{"Country":"Argentina","City":"El Palomar Aerodrome"},{"Country":"Argentina","City":"Ezeiza Aerodrome"},{"Country":"Argentina","City":"Mendoza Aerodrome"},{"Country":"Argentina","City":"San Martin"},{"Country":"Argentina","City":"Jachal"},{"Country":"Argentina","City":"Malargue Aerodrome"},{"Country":"Argentina","City":"San Rafael Aerodrome"},{"Country":"Argentina","City":"San Carlos"},{"Country":"Argentina","City":"Uspallata"},{"Country":"Argentina","City":"Catamarca Aero."},{"Country":"Argentina","City":"Santiago Del Estero Aero."},{"Country":"Argentina","City":"Tinogasta"},{"Country":"Argentina","City":"La Rioja Aero."},{"Country":"Argentina","City":"Chilecito"},{"Country":"Argentina","City":"Tucuman Aerodrome"},{"Country":"Argentina","City":"San Juan Aerodrome"},{"Country":"Argentina","City":"Ceres Aerodrome"},{"Country":"Argentina","City":"Rio Cuarto Aerodrome"},{"Country":"Argentina","City":"Villa Dolores Aerodrome"},{"Country":"Argentina","City":"Laboulaye"},{"Country":"Argentina","City":"Marcos Juarez Aerodrome"},{"Country":"Argentina","City":"Villa Reynolds Aerodrome"},{"Country":"Argentina","City":"San Luis Aerodrome"},{"Country":"Argentina","City":"Corrientes Aero."},{"Country":"Argentina","City":"Resistencia Aero."},{"Country":"Argentina","City":"Formosa Aerodrome"},{"Country":"Argentina","City":"Iguazu Aerodrome"},{"Country":"Argentina","City":"Paso De Los Libres Aerodrome"},{"Country":"Argentina","City":"Monte Caseros Aerodrome"},{"Country":"Argentina","City":"Posadas Aero."},{"Country":"Argentina","City":"Presidencia Roque Saenz Pena Aer"},{"Country":"Argentina","City":"Salta Aerodrome"},{"Country":"Argentina","City":"Jujuy Aerodrome"},{"Country":"Argentina","City":"Oran Aerodrome"},{"Country":"Argentina","City":"La Quiaca Observatorio"},{"Country":"Argentina","City":"Rivadavia"},{"Country":"Argentina","City":"Tartagal Aerodrome"},{"Country":"Argentina","City":"Las Lomitas"},{"Country":"Argentina","City":"Curuzu Cuatia Aerodrome"},{"Country":"Argentina","City":"El Bolson Aerodrome"},{"Country":"Argentina","City":"Comodoro Rivadavia Aerodrome"},{"Country":"Argentina","City":"Esquel Aerodrome"},{"Country":"Argentina","City":"San Antonio Oeste Aerodrome"},{"Country":"Argentina","City":"Paso De Indios"},{"Country":"Argentina","City":"Trelew Aerodrome"},{"Country":"Argentina","City":"Viedma Aerodrome"},{"Country":"Argentina","City":"Lago Argentino Aerodrome"},{"Country":"Argentina","City":"Puerto Deseado Aerodrome"},{"Country":"Argentina","City":"Rio Grande B. A."},{"Country":"Argentina","City":"Rio Gallegos Aerodrome"},{"Country":"Argentina","City":"Ushuaia Aerodrome"},{"Country":"Argentina","City":"San Julian Aerodrome"},{"Country":"Argentina","City":"Perito Moreno Aerodrome"},{"Country":"Argentina","City":"Gobernador Gregores Aerodrome"},{"Country":"Argentina","City":"Santa Cruz Aerodrome"},{"Country":"Argentina","City":"Azul Airport"},{"Country":"Argentina","City":"Bahia Blanca Aerodrome"},{"Country":"Argentina","City":"Dolores Aerodrome"},{"Country":"Argentina","City":"Pigue Aerodrome"},{"Country":"Argentina","City":"General Pico Aerodrome"},{"Country":"Argentina","City":"Tres Arroyos"},{"Country":"Argentina","City":"Mar Del Plata Aerodrome"},{"Country":"Argentina","City":"Neuquen Aerodrome"},{"Country":"Argentina","City":"Pehuajo Aerodrome"},{"Country":"Argentina","City":"Rio Colorado"},{"Country":"Argentina","City":"Santa Rosa Aerodrome"},{"Country":"Argentina","City":"Bariloche Aerodrome"},{"Country":"Argentina","City":"Tandil Aerodrome"},{"Country":"Argentina","City":"Villa Gesell"},{"Country":"Argentina","City":"Chapelco"},{"Country":"Falkland Islands (Malvinas)","City":"Stanley Airport"},{"Country":"Suriname","City":"Johan A. Pengel"},{"Country":"Suriname","City":"Zanderij"},{"Country":"Dominica","City":"Canefield Airport"},{"Country":"Dominica","City":"Melville Hall Airport"},{"Country":"Dominica","City":"Roseau"},{"Country":"Martinique","City":"Le Lamentin"},{"Country":"Virgin Islands, U.S.","City":"Charlotte Amalie, Cyril E. King International Airport, Saint Thomas"},{"Country":"Saint Kitts and Nevis","City":"Golden Rock"},{"Country":"Saint Lucia","City":"Vigie"},{"Country":"Saint Lucia","City":"Hewanorra International Airport"},{"Country":"Trinidad and Tobago","City":"Crown Pt./ Scarborou"},{"Country":"Trinidad and Tobago","City":"Piarco International Airport, Trinidad"},{"Country":"Trinidad and Tobago","City":"Crown Point Airport, Tobago"},{"Country":"Virgin Islands, British","City":"Beef Island, Tortola"},{"Country":"Saint Vincent and the Grenadines","City":"Arnos Vale"},{"Country":"Ukraine","City":"Boryspil"},{"Country":"Ukraine","City":"Simferopol"},{"Country":"Ukraine","City":"Kharkiv"},{"Country":"Ukraine","City":"Kyiv"},{"Country":"Ukraine","City":"L Viv"},{"Country":"Ukraine","City":"Odesa"},{"Country":"India","City":"Ahmadabad"},{"Country":"India","City":"Akola"},{"Country":"India","City":"Aurangabad Chikalthan Aerodrome"},{"Country":"India","City":"Bombay / Santacruz"},{"Country":"India","City":"Bilaspur"},{"Country":"India","City":"Bhuj-Rudramata"},{"Country":"India","City":"Belgaum / Sambra"},{"Country":"India","City":"Bhopal / Bairagarh"},{"Country":"India","City":"Bhaunagar"},{"Country":"India","City":"Goa / Dabolim Airport"},{"Country":"India","City":"Indore"},{"Country":"India","City":"Jabalpur"},{"Country":"India","City":"Khandwa"},{"Country":"India","City":"Kolhapur"},{"Country":"India","City":"Nagpur Sonegaon"},{"Country":"India","City":"Rajkot"},{"Country":"India","City":"Sholapur"},{"Country":"India","City":"Agartala"},{"Country":"India","City":"Siliguri"},{"Country":"India","City":"Bhubaneswar"},{"Country":"India","City":"Calcutta / Dum Dum"},{"Country":"India","City":"Car Nicobar"},{"Country":"India","City":"Gorakhpur"},{"Country":"India","City":"Gauhati"},{"Country":"India","City":"Gaya"},{"Country":"India","City":"Imphal Tulihal"},{"Country":"India","City":"Jharsuguda"},{"Country":"India","City":"Jamshedpur"},{"Country":"India","City":"North Lakhimpur"},{"Country":"India","City":"Dibrugarh / Mohanbari"},{"Country":"India","City":"Port Blair"},{"Country":"India","City":"Patna"},{"Country":"India","City":"M. O. Ranchi"},{"Country":"India","City":"Agra"},{"Country":"India","City":"Allahabad / Bamhrauli"},{"Country":"India","City":"Amritsar"},{"Country":"India","City":"Varanasi / Babatpur"},{"Country":"India","City":"Bareilly"},{"Country":"India","City":"Kanpur / Chakeri"},{"Country":"India","City":"New Delhi / Safdarjung"},{"Country":"India","City":"New Delhi / Palam"},{"Country":"India","City":"Gwalior"},{"Country":"India","City":"Hissar"},{"Country":"India","City":"Jhansi"},{"Country":"India","City":"Jodhpur"},{"Country":"India","City":"Jaipur / Sanganer"},{"Country":"India","City":"Kota Aerodrome"},{"Country":"India","City":"Lucknow / Amausi"},{"Country":"India","City":"Satna"},{"Country":"India","City":"Udaipur Dabok"},{"Country":"India","City":"Bellary"},{"Country":"India","City":"Vijayawada / Gannavaram"},{"Country":"India","City":"Coimbatore / Peelamedu"},{"Country":"India","City":"Cochin / Willingdon"},{"Country":"India","City":"Cuddapah"},{"Country":"India","City":"Hyderabad Airport"},{"Country":"India","City":"Madurai"},{"Country":"India","City":"Mangalore / Bajpe"},{"Country":"India","City":"Madras / Minambakkam"},{"Country":"India","City":"Tiruchchirapalli"},{"Country":"India","City":"Thiruvananthapuram"},{"Country":"India","City":"Vellore"},{"Country":"Indonesia","City":"Ujung Pandang / Hasanuddin"},{"Country":"Indonesia","City":"Bau-Bau / Beto Ambiri"},{"Country":"Indonesia","City":"Kendari / Woltermon-Ginsidi"},{"Country":"Indonesia","City":"Biak / Mokmer"},{"Country":"Indonesia","City":"Nabire"},{"Country":"Indonesia","City":"Kokonao / Timuka"},{"Country":"Indonesia","City":"Serui / Yendosa"},{"Country":"Indonesia","City":"Enarotali"},{"Country":"Indonesia","City":"Sarmi"},{"Country":"Indonesia","City":"Jayapura / Sentani"},{"Country":"Indonesia","City":"Wamena / Wamena"},{"Country":"Indonesia","City":"Merauke / Mopah"},{"Country":"Indonesia","City":"Tanah Merah / Tanah Merah"},{"Country":"Indonesia","City":"Galela / Gamarmalamu"},{"Country":"Indonesia","City":"Gorontalo / Jalaluddin"},{"Country":"Indonesia","City":"Tahuna"},{"Country":"Indonesia","City":"Toli-Toli / Lalos"},{"Country":"Indonesia","City":"Palu / Mutiara"},{"Country":"Indonesia","City":"Menado / Dr. Sam Ratulangi"},{"Country":"Indonesia","City":"Poso / Kasiguncu"},{"Country":"Indonesia","City":"Ternate / Babullah"},{"Country":"Indonesia","City":"Luwuk / Bubung"},{"Country":"Indonesia","City":"Amahai"},{"Country":"Indonesia","City":"Labuha / Taliabu"},{"Country":"Indonesia","City":"Saumlaki"},{"Country":"Indonesia","City":"Sanana"},{"Country":"Indonesia","City":"Ambon / Pattimura"},{"Country":"Indonesia","City":"Namlea"},{"Country":"Indonesia","City":"Fak-Fak / Torea"},{"Country":"Indonesia","City":"Kaimana / Utarom"},{"Country":"Indonesia","City":"Manokwari / Rendani"},{"Country":"Indonesia","City":"Sorong / Jefman"},{"Country":"Indonesia","City":"Sabang / Cut Bau"},{"Country":"Indonesia","City":"Menggala / Astra Ksetra"},{"Country":"Indonesia","City":"Tasikmalaya / Cibeureum"},{"Country":"Indonesia","City":"Madiun / Iswahyudi"},{"Country":"Indonesia","City":"Malang / Abdul Rahkmansaleh"},{"Country":"Indonesia","City":"Pakanbaru / Simpangtiga"},{"Country":"Indonesia","City":"Curug / Budiarto"},{"Country":"Indonesia","City":"Bandung / Husein"},{"Country":"Indonesia","City":"Jakarta Halim Perdanakusuma"},{"Country":"Indonesia","City":"Jakarta / Soekarno-Hatta"},{"Country":"Indonesia","City":"Jogyakarta / Adisucipto"},{"Country":"Indonesia","City":"Kalijati"},{"Country":"Indonesia","City":"Cilacap"},{"Country":"Indonesia","City":"Semarang / Ahmadyani"},{"Country":"Indonesia","City":"Telukbetung / Beranti"},{"Country":"Indonesia","City":"Tanjungpandan / Buluh Tumbang"},{"Country":"Indonesia","City":"Pangkalpinang / Pangkalpinang"},{"Country":"Indonesia","City":"Tanjungpinang / Kijang"},{"Country":"Indonesia","City":"Singkep / Dabo"},{"Country":"Indonesia","City":"Gunung Sitoli / Binaka"},{"Country":"Indonesia","City":"Padang / Tabing"},{"Country":"Indonesia","City":"Medan / Polonia"},{"Country":"Indonesia","City":"Sibolga / Pinangsori"},{"Country":"Indonesia","City":"Singkawang Ii"},{"Country":"Indonesia","City":"Ketapang / Rahadi Usmaman"},{"Country":"Indonesia","City":"Ranai / Ranai"},{"Country":"Indonesia","City":"Pontianak / Supadio"},{"Country":"Indonesia","City":"Sintang"},{"Country":"Indonesia","City":"Jambi / Sultan Taha"},{"Country":"Indonesia","City":"Kerinci / Depati Parbo"},{"Country":"Indonesia","City":"Bengkulu / Padangkemiling"},{"Country":"Indonesia","City":"Palembang / Talangbetutu"},{"Country":"Indonesia","City":"Rengat / Japura"},{"Country":"Indonesia","City":"Meulaboh / Cut Nyak Dhien"},{"Country":"Indonesia","City":"Lhokseumawe / Malikussaleh"},{"Country":"Indonesia","City":"Banda Aceh / Blangbintang"},{"Country":"Indonesia","City":"Banjarmasin / Syamsuddin Noor"},{"Country":"Indonesia","City":"Pangkalan Bun / Iskandar"},{"Country":"Indonesia","City":"Kotabaru"},{"Country":"Indonesia","City":"Muaratewe / Beringin"},{"Country":"Indonesia","City":"Palangkaraya / Panarung"},{"Country":"Indonesia","City":"Maumere / Wai Oti"},{"Country":"Indonesia","City":"Kupang / El Tari"},{"Country":"Indonesia","City":"Larantuka"},{"Country":"Indonesia","City":"Alor / Mali"},{"Country":"Indonesia","City":"Rote / Baa"},{"Country":"Indonesia","City":"Sabu / Tardamu"},{"Country":"Indonesia","City":"Longbawan / Juvai Semaring"},{"Country":"Indonesia","City":"Tanjung Selor"},{"Country":"Indonesia","City":"Tanjung Redep / Berau"},{"Country":"Indonesia","City":"Balikpapan / Sepinggan"},{"Country":"Indonesia","City":"Tarakan / Juwata"},{"Country":"Indonesia","City":"Samarinda / Temindung"},{"Country":"Indonesia","City":"Ampenan / Selaparang"},{"Country":"Indonesia","City":"Bima"},{"Country":"Indonesia","City":"Denpasar / Ngurah-Rai"},{"Country":"Indonesia","City":"Sumbawa Besar / Sumbawa Besar"},{"Country":"Indonesia","City":"Waingapu / Mau Hau"},{"Country":"Indonesia","City":"Surabaya / Juanda"},{"Country":"Indonesia","City":"Surabaya / Perak"},{"Country":"Indonesia","City":"Surakarta / Adisumarmo"},{"Country":"Indonesia","City":"Surabaya"},{"Country":"Singapore","City":"Singapore / Paya Lebar"},{"Country":"Singapore","City":"Singapore / Changi Airport"},{"Country":"China","City":"Beijing"},{"Country":"China","City":"Hohhot"},{"Country":"China","City":"Tianjin / Zhangguizhu"},{"Country":"China","City":"Taiyuan"},{"Country":"China","City":"Changsha"},{"Country":"China","City":"Guangzhou"},{"Country":"China","City":"Haikou"},{"Country":"China","City":"Guilin"},{"Country":"China","City":"Nanning"},{"Country":"China","City":"Shantou"},{"Country":"China","City":"Shenzhen"},{"Country":"China","City":"Zhanjiang"},{"Country":"China","City":"Zhengzhou"},{"Country":"China","City":"Wuhan"},{"Country":"China","City":"Yinchuan"},{"Country":"China","City":"Jiuquan"},{"Country":"China","City":"Xi An"},{"Country":"China","City":"Xining"},{"Country":"China","City":"Yan An"},{"Country":"China","City":"Kunming"},{"Country":"China","City":"Xiamen"},{"Country":"China","City":"Nanchang"},{"Country":"China","City":"Fuzhou"},{"Country":"China","City":"Ganzhou"},{"Country":"China","City":"Hangzhou"},{"Country":"China","City":"Nanjing"},{"Country":"China","City":"Hefei"},{"Country":"China","City":"Qingdao"},{"Country":"China","City":"Shanghai / Hongqiao"},{"Country":"China","City":"Jinan"},{"Country":"China","City":"Chongqing"},{"Country":"China","City":"Guiyang"},{"Country":"China","City":"Lhasa"},{"Country":"China","City":"Chengdu"},{"Country":"China","City":"Hami"},{"Country":"China","City":"Kashi"},{"Country":"China","City":"Hotan"},{"Country":"China","City":"Urum-Qi / Diwopu"},{"Country":"China","City":"Yining"},{"Country":"China","City":"Changchun"},{"Country":"China","City":"Qiqihar"},{"Country":"China","City":"Dalian"},{"Country":"Papua New Guinea","City":"Madang"},{"Country":"Papua New Guinea","City":"Moresby"},{"Country":"Papua New Guinea","City":"Wewak"}]}}');
    var rawCitiesResponse = JSON.parse('{"NewDataSet":{"Table":[{"Country":"British Indian Ocean Territory","City":"Diego Garcia"},{"Country":"India","City":"Ahmadabad"},{"Country":"India","City":"Akola"},{"Country":"India","City":"Aurangabad Chikalthan Aerodrome"},{"Country":"India","City":"Bombay / Santacruz"},{"Country":"India","City":"Bilaspur"},{"Country":"India","City":"Bhuj-Rudramata"},{"Country":"India","City":"Belgaum / Sambra"},{"Country":"India","City":"Bhopal / Bairagarh"},{"Country":"India","City":"Bhaunagar"},{"Country":"India","City":"Goa / Dabolim Airport"},{"Country":"India","City":"Indore"},{"Country":"India","City":"Jabalpur"},{"Country":"India","City":"Khandwa"},{"Country":"India","City":"Kolhapur"},{"Country":"India","City":"Nagpur Sonegaon"},{"Country":"India","City":"Rajkot"},{"Country":"India","City":"Sholapur"},{"Country":"India","City":"Agartala"},{"Country":"India","City":"Siliguri"},{"Country":"India","City":"Bhubaneswar"},{"Country":"India","City":"Calcutta / Dum Dum"},{"Country":"India","City":"Car Nicobar"},{"Country":"India","City":"Gorakhpur"},{"Country":"India","City":"Gauhati"},{"Country":"India","City":"Gaya"},{"Country":"India","City":"Imphal Tulihal"},{"Country":"India","City":"Jharsuguda"},{"Country":"India","City":"Jamshedpur"},{"Country":"India","City":"North Lakhimpur"},{"Country":"India","City":"Dibrugarh / Mohanbari"},{"Country":"India","City":"Port Blair"},{"Country":"India","City":"Patna"},{"Country":"India","City":"M. O. Ranchi"},{"Country":"India","City":"Agra"},{"Country":"India","City":"Allahabad / Bamhrauli"},{"Country":"India","City":"Amritsar"},{"Country":"India","City":"Varanasi / Babatpur"},{"Country":"India","City":"Bareilly"},{"Country":"India","City":"Kanpur / Chakeri"},{"Country":"India","City":"New Delhi / Safdarjung"},{"Country":"India","City":"New Delhi / Palam"},{"Country":"India","City":"Gwalior"},{"Country":"India","City":"Hissar"},{"Country":"India","City":"Jhansi"},{"Country":"India","City":"Jodhpur"},{"Country":"India","City":"Jaipur / Sanganer"},{"Country":"India","City":"Kota Aerodrome"},{"Country":"India","City":"Lucknow / Amausi"},{"Country":"India","City":"Satna"},{"Country":"India","City":"Udaipur Dabok"},{"Country":"India","City":"Bellary"},{"Country":"India","City":"Vijayawada / Gannavaram"},{"Country":"India","City":"Coimbatore / Peelamedu"},{"Country":"India","City":"Cochin / Willingdon"},{"Country":"India","City":"Cuddapah"},{"Country":"India","City":"Hyderabad Airport"},{"Country":"India","City":"Madurai"},{"Country":"India","City":"Mangalore / Bajpe"},{"Country":"India","City":"Madras / Minambakkam"},{"Country":"India","City":"Tiruchchirapalli"},{"Country":"India","City":"Thiruvananthapuram"},{"Country":"India","City":"Vellore"}]}}');
    var weatherResponse = JSON.parse('{"CurrentWeather":{"Location":"NSF DIEGO GARCIA, IO, British Indian Ocean Territory (FJDG) 7-18S 72-24E 3M","Time":"Mar 24, 2016 - 05:55 AM EDT / 2016.03.24 0955 UTC","Wind":"from the W (270 degrees) at 7 MPH (6 KT):0","Visibility":"greater than 7 mile(s):0","SkyConditions":"partly cloudy","Temperature":"89.6 F (32.0 C)","DewPoint":"81.3 F (27.4 C)","RelativeHumidity":"76%","Pressure":"29.77 in. Hg (1008 hPa)","Status":"Success"}}');

    beforeEach(module('globalweather'));

    beforeEach(function () {
        inject(function (_$controller_, _$rootScope_, _$q_) {
            scope = _$rootScope_.$new();
            citiesDeferred = _$q_.defer();
            weatherDeferred = _$q_.defer();

            mockWeatherService = {
                getCities: function (a) {
                    return citiesDeferred.promise;
                },
                getCountries: function (b) {
                    return defer.promise;
                },
                getWeather: function (a, b) {
                    return weatherDeferred.promise;
                }
            };

            spyOn(mockWeatherService, 'getCities').and.callThrough();
            spyOn(mockWeatherService, 'getWeather').and.callThrough();

            controller = _$controller_('weatherController', {
                $scope: scope,
                weatherService: mockWeatherService
            });
        });
    });

    describe('on load', function () {
        it('should set page title', function () {
            expect(scope.pageTitle).toBe('Weather Information');
        });

        it('should provide an empty weather json', function () {
            expect(scope.weather).toEqual({});
        });
    });

    describe('on selecting country', function () {
        beforeEach(function () {
            scope.selectCountry('India');
        });

        it('should set selected country on scope', function () {
            expect(scope.selectedCountry).toEqual('India');
        });

        it('should clear cities collection', function () {
            expect(scope.countries.length).toEqual(0);
        });

        it('should invoke weather service to fetch cities', function () {
            expect(mockWeatherService.getCities).toHaveBeenCalledWith('India');
        });

        describe('when the service returns empty list', function () {
            beforeEach(function () {
                scope.$apply(function () {
                    citiesDeferred.resolve({ NewDataSet: { Table: [] } });
                });
            });

            it('should show error message', function () {
                expect(scope.error).toBe('No data found. Please try again later.');
            });

            it('should not show city section', function () {
                expect(scope.showCity()).toBe(false);
            });

            it('should not show city list', function () {
                expect(scope.showCityList()).toBe(false);
            });
        });

        describe('on successful fetch of cities', function () {
            beforeEach(function () {
                scope.$apply(function () {
                    citiesDeferred.resolve(rawCitiesResponse);
                });
            });

            it('should fetch collection of cities for the selected country', function () {
                expect(scope.cities[0]).toEqual('Diego Garcia');
            });

            it('should show city section', function () {
                expect(scope.showCity()).toBe(true);
            });

            it('should show city list', function () {
                expect(scope.showCityList()).toBe(true);
            });

            describe('on selection of city', function () {
                beforeEach(function () {
                    scope.selectCity('Diego Garcia');
                });

                it('should set the selected city on scope', function () {
                    expect(scope.selectedCity).toEqual('Diego Garcia');
                });

                it('should invoke weather service to fetch weather information', function () {
                    expect(mockWeatherService.getWeather).toHaveBeenCalledWith({ country: 'India', city: 'Diego Garcia' });
                });

                it('should show city section', function () {
                    expect(scope.showCity()).toBe(true);
                });

                it('should not show city list', function () {
                    expect(scope.showCityList()).toBe(false);
                });

                describe('when the service returns no/empty weather information', function () {
                    beforeEach(function () {
                        scope.$apply(function () {
                            weatherDeferred.resolve({ CurrentWeather: null });
                        });
                    });

                    it('should show error message', function () {
                        expect(scope.error).toBe('No data found. Please try again later.');
                    });

                    it('should not show weather info section', function () {
                        expect(scope.showWeather()).toBe(false);
                    });
                });

                describe('on successful fetch of weather information', function () {
                    beforeEach(function () {
                        scope.$apply(function () {
                            weatherDeferred.resolve(weatherResponse);
                        });
                    });

                    it('should map the details to scope', function () {
                        var detail = scope.weather.details[0];
                        expect(detail.label).toEqual('Location');
                        expect(detail.info).toEqual('NSF DIEGO GARCIA, IO, British Indian Ocean Territory (FJDG) 7-18S 72-24E 3M');
                    });

                    it('should show weather info section', function () {
                        expect(scope.showWeather()).toBe(true);
                    });

                    describe('on focus of city', function () {
                        beforeEach(function () {
                            scope.resetCity();
                        });

                        it('should not show weather info section', function () {
                            expect(scope.showWeather()).toBe(false);
                        });

                        it('should show city section', function () {
                            expect(scope.showCity()).toBe(true);
                        });

                        it('should show city list', function () {
                            expect(scope.showCityList()).toBe(true);
                        });

                        it('should reset city', function () {
                            expect(scope.weather.city).toEqual('');
                        });
                    });

                    describe('on focus of country', function () {
                        beforeEach(function () {
                            scope.resetCountry();
                        });

                        it('should not show weather info section', function () {
                            expect(scope.showWeather()).toBe(false);
                        });

                        it('should not show city section', function () {
                            expect(scope.showCity()).toBe(false);
                        });

                        it('should not show city list', function () {
                            expect(scope.showCityList()).toBe(false);
                        });

                        it('should reset city', function () {
                            expect(scope.weather.city).toEqual('');
                        });

                        it('should reset country', function () {
                            expect(scope.weather.country).toEqual('');
                        });

                        it('should not show country list', function () {
                            expect(scope.showCountryList()).toBe(false);
                        });
                    });
                });

                describe('on failure to fetch from weather information', function () {
                    beforeEach(function () {
                        scope.$apply(function () {
                            weatherDeferred.reject({});
                        });
                    });

                    it('should show error message', function () {
                        expect(scope.error).toBe('Something went wrong! Please try again later');
                    });

                    it('should reset cities collection', function () {
                        expect(scope.weather.details.length).toEqual(0);
                    });

                    it('should not show weather info section', function () {
                        expect(scope.showWeather()).toBe(false);
                    });
                });
            });
        });

        describe('on failure to fetch cities', function () {
            beforeEach(function () {
                scope.$apply(function () {
                    citiesDeferred.reject({});
                    scope.cities = [{}];
                });
            });

            it('should show error message', function () {
                expect(scope.error).toBe('Something went wrong! Please try again later');
            });

            it('should reset cities collection', function () {
                expect(scope.cities.length).toEqual(0);
            });
        });
    });
});